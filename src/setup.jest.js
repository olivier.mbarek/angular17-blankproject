import 'jest-preset-angular/setup-jest.js'

module.exports = {
  coverageDirectory: './coverage',
  coverageReporters: ['clover', 'json', 'lcov', 'text', 'text-summary'],
  collectCoverage: true,
  testResultsProcessor: 'jest-sonar-reporter'
}
